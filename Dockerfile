FROM rouault/gdal-mdim-cog

RUN apt update  -y                          && \
    apt install -y python3-pip git

RUN mkdir /tmp/mcog
COPY . /tmp/mcog

RUN pip install -r /tmp/mcog/requirements.txt    

RUN cd /tmp/mcog                            && \
    python setup.py install                 && \
    cd -                                    && \
    rm -fr /tmp/cog

