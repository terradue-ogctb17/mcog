# GDAL Multi-dimension COG creation

This repo contains a demonstrator for the creation of a multi-dimension COG file.

## Generating the MD-COG with CWL

The Common Workflow Language `workflow.cwl` orchestrates the tools required for the generation of a MD COG file with three Sentinel-2 Normalized Burn Ratio (NBNR) over the Kangaroo island in Australia.

To run this workflow, you need `docker` and a CWL runner e.g. `cwltool`

First build the container image with the python project that generates the MD COG with:

```
docker build -t md-cog .
```

Then simply do: 

```console
cwltool --parallel workflow.cwl params.yml
```

This generates a MD COG file called `md.tif`

## Generating the MD-COG with the released CWL

Copy the released `workflow.<major>.<minor>.<maintenance>.cwl' file URL available in repository published packages (https://gitlab.com/terradue-ogctb17/mcog/-/packages) and run it with, e.g.:

```console
cwltool --parallel https://gitlab.com/terradue-ogctb17/mcog/-/package_files/17594017/download params.yml
```

## Inspect the MD COG with a Notebook (requires VS Code)

This repo includes a VS Code development container configuration providing a notebook to inspect the MD COG file generated.

Use VS Code and start the development container.
