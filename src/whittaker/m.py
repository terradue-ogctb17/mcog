from osgeo import gdal
import array
import numpy

def test_gtiff_mdim_basic_4D(blockSize, arraySize):

    filename = 'mdim.tif'
    dt = gdal.GDT_UInt16
    t_values = [i + 1 for i in range(arraySize[0])]
    z_values = [i + 1 for i in range(arraySize[1])]

    def write():
        ds = gdal.GetDriverByName('GTiff').CreateMultiDimensional(filename)
        rg = ds.GetRootGroup()
        dimT = rg.CreateDimension("dimT", None, None, arraySize[0])
        dimZ = rg.CreateDimension("dimZ", 'a', 'b', arraySize[1])
        dimY = rg.CreateDimension("dimY", None, None, arraySize[2])
        dimX = rg.CreateDimension("dimX", None, None, arraySize[3])

        dimTVar = rg.CreateMDArray("dimT", [dimT], gdal.ExtendedDataType.Create(
            gdal.GDT_Int32), ['IS_INDEXING_VARIABLE=YES'])
        dimT.SetIndexingVariable(dimTVar)
        dimTVar.Write(array.array('f', t_values))

        dimZVar = rg.CreateMDArray("dimZ", [dimZ], gdal.ExtendedDataType.Create(
            gdal.GDT_Int32), ['IS_INDEXING_VARIABLE=YES'])
        dimZ.SetIndexingVariable(dimZVar)
        dimZVar.Write(array.array('f', z_values))

        ar = rg.CreateMDArray("myarray", [dimT, dimZ, dimY, dimX],
                              gdal.ExtendedDataType.Create(dt),
                              ['BLOCKSIZE=%d,%d,%d,%d' % (blockSize[0], blockSize[1], blockSize[2], blockSize[3])])
        numpy_ar = numpy.reshape(numpy.arange(0, dimT.GetSize() * dimZ.GetSize() * dimY.GetSize() * dimX.GetSize(), dtype=numpy.uint16),
                                 (dimT.GetSize(), dimZ.GetSize(), dimY.GetSize(), dimX.GetSize()))
        if dt == gdal.GDT_Byte:
            numpy_ar = numpy.clip(numpy_ar, 0, 255)
        assert ar.Write(numpy_ar) == gdal.CE_None
        
        #ds.flush()
        return numpy_ar

    ref_ar = write()
 