import sys
import logging
import click

from .m import test_gtiff_mdim_basic_4D

logging.basicConfig(
    stream=sys.stderr,
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)-8s %(message)s",
    datefmt="%Y-%m-%dT%H:%M:%S",
)


@click.command(
    short_help="mcog - multi-dimension COG",
    help="mcog - multi-dimension COG",
    context_settings=dict(
        ignore_unknown_options=True,
        allow_extra_args=True,
    ),
)
@click.option(
    "--input_path",
    "-i",
    "input_path",
    help="An input reference",
    type=click.Path(),
    required=True,
)
@click.pass_context
def main(ctx, input_path):

    logging.info("Hello World!")

    

    test_gtiff_mdim_basic_4D((2, 3, 16, 32), (5, 8, 32, 64))


    sys.exit(0)
    
if __name__ == "__main__":
    main()
