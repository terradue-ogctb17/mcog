from osgeo import gdal
import array
import numpy
import os
import sys
import logging

logging.basicConfig(
    stream=sys.stderr,
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)-8s %(message)s",
    datefmt="%Y-%m-%dT%H:%M:%S",
)


def input_to_date(item):
    # weak but ok
    return int(os.path.basename(item)[10:18])


def gtiff_mdim_basic_4D(input_items, output_tif):

    for tif_item in input_items:
        # item=os.path.join(input_image,tif_item)
        logging.info("Item: {}".format(tif_item))
        ds = gdal.Open(tif_item)
        logging.info("size is : {}x{}".format(ds.RasterXSize, ds.RasterYSize))

    block_size = (1, 1, 256, 512)

    array_size = (len(input_items), 1, ds.RasterYSize, ds.RasterXSize)

    filename = output_tif
    dt = gdal.GDT_Float32
    # prepare data to be appregated

    # input_items=[os.path.basename(x) for x in os.listdir(input_dir) if x.split('.')[-1]=='tif']
    dataq = []
    dates = []
    for item in input_items:
        ds = gdal.Open(item)
        data = ds.GetRasterBand(1).ReadAsArray()
        dataq.append(data[: array_size[2], : array_size[3]])
        dates.append(input_to_date(item))

    x = numpy.stack(dataq)
    # print('shape of data to be written to nbr:{}'.format(x.shape))
    logging.info("shape of data to be written to nbr:{}".format(x.shape))
    logging.info("Dates:{}".format(dates))
    X = ds.RasterXSize  # cols
    Y = ds.RasterYSize  # rows
    transform = ds.GetGeoTransform()

    xOrigin = transform[0]
    yOrigin = transform[3]
    pixelWidth = transform[1]
    pixelHeight = -transform[5]

    logging.info("XRaster - cols is {}".format(X))

    logging.info("YRaster - rows is {}".format(Y))
    if X < array_size[3] or Y < array_size[2]:
        print("Error: subset is bigger than the image extents.")
        raise AssertionError()

    # coordinate X , Y
    idx_x = []
    idx_y = []
    for i in range(array_size[3]):
        idx_x.append(((i + 0.5) * pixelWidth) + xOrigin)

    for j in range(array_size[2]):
        idx_y.append(((j + 0.5) * pixelHeight) + yOrigin)

    def write():
        ds = gdal.GetDriverByName("GTiff").CreateMultiDimensional(filename)
        rg = ds.GetRootGroup()

        dimT = rg.CreateDimension("Time", "TEMPORAL", None, array_size[0])
        dimZ = rg.CreateDimension("NBR", "d", None, array_size[1])
        dimY = rg.CreateDimension("Latitude", "HORIZONTAL_Y", None, array_size[2])
        dimX = rg.CreateDimension("Longitude", "HORIZONTAL_X", None, array_size[3])

        # print(len(t_values),arraySize[0])
        assert len(dates) == array_size[0]
        dimTVar = rg.CreateMDArray(
            "Time",
            [dimT],
            gdal.ExtendedDataType.Create(gdal.GDT_Int32),
            ["IS_INDEXING_VARIABLE=YES"],
        )
        dimT.SetIndexingVariable(dimTVar)
        dimTVar.Write(array.array("f", dates))

        assert len(idx_x) == array_size[3]
        dimXVar = rg.CreateMDArray(
            "Lon",
            [dimX],
            gdal.ExtendedDataType.Create(gdal.GDT_Int32),
            ["IS_INDEXING_VARIABLE=YES"],
        )
        dimX.SetIndexingVariable(dimXVar)
        dimXVar.Write(array.array("f", idx_x))

        assert len(idx_y) == array_size[2]
        dimYVar = rg.CreateMDArray(
            "Lat",
            [dimY],
            gdal.ExtendedDataType.Create(gdal.GDT_Int32),
            ["IS_INDEXING_VARIABLE=YES"],
        )
        dimY.SetIndexingVariable(dimYVar)
        dimYVar.Write(array.array("f", idx_y))

        ar = rg.CreateMDArray(
            "myarray",
            [dimT, dimZ, dimY, dimX],
            gdal.ExtendedDataType.Create(dt),
            [
                "BLOCKSIZE=%d,%d,%d,%d"
                % (block_size[0], block_size[1], block_size[2], block_size[3])
            ],
        )

        numpy_ar = numpy.reshape(
            x, (dimT.GetSize(), dimZ.GetSize(), dimY.GetSize(), dimX.GetSize())
        )

        assert ar.Write(numpy_ar) == gdal.CE_None

        print(ar.GetTotalElementsCount())
        print(ar.GetDimensionCount())

        return numpy_ar

    ref_ar = write()
