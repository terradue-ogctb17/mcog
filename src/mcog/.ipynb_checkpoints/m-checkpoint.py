from osgeo import gdal
import array
import numpy
import os


def get_block_size(band):

    return band.GetBlockSize()[0], band.GetBlockSize()[1]


def test_gtiff_mdim_basic_4D(blockSize, arraySize,input_dir):

    filename = 'mdim_subsect.tif'
    dt = gdal.GDT_Float32
    #prepare data to be appregated 
    
    input_items=[os.path.basename(x) for x in os.listdir(input_dir) if x.split('.')[-1]=='tif']
    dataq=[]
    for item in input_items:
        ds = gdal.Open(os.path.join(input_dir,item))
        data = ds.GetRasterBand(1).ReadAsArray()
        dataq.append(data[:arraySize[2],:arraySize[3]])
    x=numpy.stack(dataq)   
    print('shape of data to be written to nbr:{}'.format(x.shape))

   
    X=ds.RasterXSize #cols
    Y=ds.RasterYSize #rows
    transform = ds.GetGeoTransform()

    xOrigin = transform[0]
    yOrigin = transform[3]
    pixelWidth = transform[1]
    pixelHeight = -transform[5]
    print('XRaster - cols is {}'.format(X))
    print('YRaster - rows is {}'.format(Y))

    if(X < arraySize[2] or Y < arraySize[3]) :
        print('Error: subset is bigger than the image extents.')
        raise AssertionError()


    # coordinate X , Y 
    idx_x=[]
    idx_y=[]
    for i in range(arraySize[2]):
        idx_x.append(((i+0.5)*pixelWidth)+xOrigin)

    for j in range(arraySize[3]):
        idx_y.append(((j+0.5)* pixelHeight)+yOrigin)
    
    # t_values for test with 3 inputs
    t_values=[20210703,20210713,20210723]

    def write():
        ds = gdal.GetDriverByName('GTiff').CreateMultiDimensional(filename)
        rg = ds.GetRootGroup()


        dimT = rg.CreateDimension("Time", "TEMPORAL", None, arraySize[0])
        dimZ = rg.CreateDimension("NBR",  'd',"VERTICAL", arraySize[1])
        dimY = rg.CreateDimension("Latitude", "HORIZONTAL_Y", None, arraySize[2])
        dimX = rg.CreateDimension("Longitude", "HORIZONTAL_X", None, arraySize[3])

        print(len(t_values),arraySize[0])
        assert len(t_values)==arraySize[0]
        dimTVar = rg.CreateMDArray("Time", [dimT], gdal.ExtendedDataType.Create(
            gdal.GDT_Int32), ['IS_INDEXING_VARIABLE=YES'])
        dimT.SetIndexingVariable(dimTVar)
        dimTVar.Write(array.array('f', t_values))
          #gdal.ExtendedDataType.CreateString()


        assert len(idx_y)==arraySize[3]
        dimYVar = rg.CreateMDArray("Lat", [dimY], gdal.ExtendedDataType.Create(
        gdal.GDT_Int32), ['IS_INDEXING_VARIABLE=YES'])
        dimY.SetIndexingVariable(dimYVar)
        print(dimYVar.GetBlockSize())
        print('Before1')
        dimYVar.Write(array.array('f', idx_y))
        print('After1')
        assert len(idx_x)==arraySize[2]
        dimXVar = rg.CreateMDArray("Lon", [dimX], gdal.ExtendedDataType.Create(
        gdal.GDT_Int32), ['IS_INDEXING_VARIABLE=YES'])
        dimX.SetIndexingVariable(dimXVar)
        print('Before2')
        dimXVar.Write(array.array('f', idx_x))
        print('After2')


        
        ar = rg.CreateMDArray("myarray", [dimT, dimZ, dimY, dimX],
                              gdal.ExtendedDataType.Create(dt),
                              ['BLOCKSIZE=%d,%d,%d,%d' % (blockSize[0], blockSize[1], blockSize[2], blockSize[3])])
 
        numpy_ar = numpy.reshape(x,(dimT.GetSize(), dimZ.GetSize(), dimY.GetSize(), dimX.GetSize()))
        
        assert ar.Write(numpy_ar) == gdal.CE_None
        #assert ar.Write(dataz,array_start_idx = [2,0, 0, 0], count = [1,1, 16, 16])  == gdal.CE_None
        print(ar.GetTotalElementsCount())
        print(ar.GetDimensionCount())


        return numpy_ar

    ref_ar = write()








