import sys
import logging
import click
import os 
from .m import test_gtiff_mdim_basic_4D
from osgeo import gdal
logging.basicConfig(
    stream=sys.stderr,
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)-8s %(message)s",
    datefmt="%Y-%m-%dT%H:%M:%S",
)


@click.command(
    short_help="mcog - multi-dimension COG",
    help="mcog - multi-dimension COG",
    context_settings=dict(
        ignore_unknown_options=True,
        allow_extra_args=True,
    ),
)
@click.option(
    "--input_path",
    "-i",
    "input_path",
    help="An input reference",
    type=click.Path(),
    required=True,
)
@click.pass_context
def main(ctx, input_path):

    logging.info("Begining of aggregation process...")
    logging.info("Input directory is: {}".format(input_path))
    input_items=[os.path.basename(x) for x in os.listdir(input_path) if x.split('.')[-1]=='tif']
    logging.info("Contents to be aggregated as a COG Multi-dim tif: {}".format(input_items))

    for tif_item in input_items:
        item=os.path.join(input_path,tif_item)
        logging.info("Item: {}".format(item))
        ds=gdal.Open(item)
        logging.info("size is : {}x{}".format(ds.RasterXSize,ds.RasterYSize))

    test_gtiff_mdim_basic_4D((1, 1, 256, 512),
    (3, 1, 650,650),input_path)

    sys.exit(0)
    
if __name__ == "__main__":
    main()