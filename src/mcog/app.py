import sys
import logging
import click
import os
from .m import gtiff_mdim_basic_4D
from osgeo import gdal

logging.basicConfig(
    stream=sys.stderr,
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)-8s %(message)s",
    datefmt="%Y-%m-%dT%H:%M:%S",
)


@click.command(
    short_help="mcog - multi-dimension COG",
    help="mcog - multi-dimension COG",
    context_settings=dict(
        ignore_unknown_options=True,
        allow_extra_args=True,
    ),
)
@click.option(
    "--input-image",
    "-im",
    "input_image",
    help="An input reference",
    type=click.Path(),
    required=True,
    multiple=True,
)
@click.option(
    "--out",
    "-o",
    "output_md_cog",
    help="The output MD COG file",
    required=True,
)
@click.pass_context
def main(ctx, input_image, output_md_cog):

    logging.info("Begining of aggregation process...")
    logging.info("Input directory is: {}".format(input_image))

    input_items = list(
        input_image
    )  # [os.path.basename(x) for x in os.listdir(input_image) if x.split('.')[-1]=='tif']

    logging.info(
        "Contents to be aggregated as a COG Multi-dim tif: {}".format(input_items)
    )

    gtiff_mdim_basic_4D(input_items, output_tif=output_md_cog)

    sys.exit(0)


if __name__ == "__main__":
    main()
