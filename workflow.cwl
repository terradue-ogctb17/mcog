$graph:
- class: Workflow
  label: NBR - produce the normalized difference between NIR and SWIR 22 over a list of stac items
  doc: NBR - produce the normalized difference between NIR and SWIR 22 over a list of stac items
  id: main

  requirements:
  - class: ScatterFeatureRequirement
  - class: SubworkflowFeatureRequirement
  inputs:
    stac_items: 
      doc: Sentinel-2 item
      type: string[]
    aoi: 
      doc: area of interest as a bounding box
      type: string
    bands: 
      type: string[]
      default: ["B8A", "B12", "SCL"]
    
  outputs:
    md_nbr:
      outputSource:
      - node_md_cog/md_cog_tif
      type: File
    
  steps:

    node_multi_nbr:

      run: "#nbr_cog_wf"

      in:
        stac_item: stac_items
        aoi: aoi
      out:
      - nbr
        
      scatter: stac_item
      scatterMethod: dotproduct 
        
    node_md_cog:

      run: "#md_cog_clt"

      in:
        tif: 
          source: node_multi_nbr/nbr

      out:
      - md_cog_tif

- class: Workflow
  label: NBR - produce the normalized difference between NIR and SWIR 22 and convert to COG
  doc: NBR - produce the normalized difference between NIR and SWIR 22 and convert to COG
  id: nbr_cog_wf

  requirements:
  - class: ScatterFeatureRequirement
  - class: SubworkflowFeatureRequirement
  inputs:
    stac_item: 
        doc: Sentinel-2 item
        type: string
    aoi: 
        doc: area of interest as a bounding box
        type: string
    bands: 
        type: string[]
        default: ["B8A", "B12", "SCL"]

  outputs:
    nbr:
      outputSource:
      - node_cog/cog_tif
      type: File

  steps:

    node_stac:

      run: "#asset_single_clt"

      in:
        stac_item: stac_item
        asset: bands

      out:
      - asset_href

      scatter: asset
      scatterMethod: dotproduct 

    node_subset:

      run: "#translate_clt"

      in: 
        asset: 
          source: node_stac/asset_href
        bbox: aoi

      out:
      - tifs

      scatter: asset
      scatterMethod: dotproduct

    node_nbr:

      run: "#band_math_clt"

      in:
        stac_item: stac_item
        tifs: 
          source: [node_subset/tifs]

      out:
      - nbr_tif
        
    node_cog:

      run: "#gdal_cog_clt"

      in:
        tif: 
          source: [node_nbr/nbr_tif]

      out:
      - cog_tif   

- class: CommandLineTool
  id: asset_single_clt

  requirements:
    DockerRequirement: 
      dockerPull: docker.io/curlimages/curl:latest
    ShellCommandRequirement: {}
    InlineJavascriptRequirement: {}

  baseCommand: [ curl, -s]
  arguments:
  - $( inputs.stac_item )

  stdout: message

  inputs:
    stac_item:
      type: string
    asset:
      type: string

  outputs:

    asset_href: 
      type: Any
      outputBinding:
        glob: message
        loadContents: true
        outputEval: ${ var assets = JSON.parse(self[0].contents).assets;
          
                      return assets[inputs.asset].href; 
                      }

- class: CommandLineTool
  id: translate_clt
  requirements:
    InlineJavascriptRequirement: {}
    DockerRequirement: 
      dockerPull: docker.io/osgeo/gdal

  baseCommand: gdal_translate
  arguments:
  - -projwin 
  - valueFrom: ${ return inputs.bbox.split(",")[0]; }
  - valueFrom: ${ return inputs.bbox.split(",")[3]; }
  - valueFrom: ${ return inputs.bbox.split(",")[2]; }
  - valueFrom: ${ return inputs.bbox.split(",")[1]; }
  - -projwin_srs
  - valueFrom: ${ return inputs.epsg; }
  - valueFrom: |
      ${ if (inputs.asset.startsWith("http")) {

           return "/vsicurl/" + inputs.asset; 

         } else { 

           return inputs.asset;

         } 
      }
  - valueFrom: ${ return inputs.asset.split("/").slice(-1)[0]; }

  inputs: 
    asset: 
      type: string
    bbox: 
      type: string
    epsg:
      type: string
      default: "EPSG:4326" 

  outputs:
    tifs:
      outputBinding:
        glob: '*.tif'
      type: File


- class: CommandLineTool
  id: band_math_clt
  requirements:
    InlineJavascriptRequirement: {}
    DockerRequirement: 
      dockerPull: docker.io/terradue/otb-7.2.0

  baseCommand: otbcli_BandMathX
  arguments: 
  - -out
  - valueFrom: ${ return inputs.stac_item.split("/").slice(-1)[0] + ".tif"; }
  - -exp
  - '(im3b1 == 8 or im3b1 == 9 or im3b1 == 0 or im3b1 == 1 or im3b1 == 2 or im3b1 == 10 or im3b1 == 11) ? -2 : (im1b1 - im2b1) / (im1b1 + im2b1)'

  inputs:

    tifs:
      type: File[]
      inputBinding:
        position: 5
        prefix: -il
        separate: true

    stac_item:
      type: string

  outputs:

    nbr_tif:
      outputBinding:
        glob: "*.tif"
      type: File

- class: CommandLineTool
  id: gdal_cog_clt
  requirements: 
    InlineJavascriptRequirement: {}
    DockerRequirement: 
      dockerPull: osgeo/gdal
  baseCommand: gdal_translate

  arguments:
  - -co 
  - COMPRESS=DEFLATE
  - -of 
  - COG
  - valueFrom: ${ return inputs.tif }
  - valueFrom: ${ return inputs.tif.basename.replace(".tif", "") + '_cog.tif'; }


  inputs:
    tif: 
      type: File

  outputs:
    cog_tif:
      outputBinding:
        glob: '*_cog.tif'
      type: File


- class: CommandLineTool
  id: md_cog_clt
  requirements: 
    InlineJavascriptRequirement: {}
    DockerRequirement: 
      dockerPull: md-cog
  baseCommand: mcog

  arguments:
  - --out
  - md.tif

  inputs:
    tif: 
      type:
        type: array
        items: File
        inputBinding:
          prefix: -im
      inputBinding:
        position: 1

  outputs:
    md_cog_tif:
      outputBinding:
        glob: 'md.tif'
      type: File

cwlVersion: v1.0

$namespaces:
  s: https://schema.org/
s:softwareVersion: 1.0.1
schemas:
- http://schema.org/version/9.0/schemaorg-current-http.rdf
